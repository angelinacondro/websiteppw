from django import forms

class Message_Form(forms.Form):
    # error_messages = {
    #     'required': 'Tolong isi input ini',
    #     'invalid': 'Isi input dengan email',
    # }
    attrs = {
        'class': 'form-control'
    }

    course_name = forms.CharField(label='Course Name', required=False, max_length=30, widget=forms.TextInput(attrs=attrs))
    course_date = forms.DateField(label = 'Couse Date', required=True, widget=forms.DateInput(attrs={'type' : 'date'}))
    course_place = forms.CharField(label = 'Course Place', required=False, max_length=30, widget=forms.TextInput(attrs=attrs))
    course_category = forms.CharField(label = 'Course Category', required=False, max_length=30, widget=forms.TextInput(attrs=attrs))
