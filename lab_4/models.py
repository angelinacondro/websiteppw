from django.db import models
from django.utils import timezone

# Create your models here.
class JadwalPribadi(models.Model):
    course_name = models.CharField(max_length=30)
    course_date = models.DateTimeField()
    course_place = models.CharField(max_length=30)
    course_category = models.CharField(max_length = 20)

