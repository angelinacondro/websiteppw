from django.conf.urls import url
from .views import index, favorite, challenge, postnew, postdelete
#url for app
urlpatterns = [
	url('index', index),
	url('favorite', favorite),
	url('challenge', challenge),
#	url('jadwal', jadwal),
	url('postnew', postnew, name='postnew'),
	url('postdelete', postdelete, name='postdelete'),
	url('', index),
]