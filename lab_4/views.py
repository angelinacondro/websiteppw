from django.shortcuts import render
from django.http import HttpResponseRedirect
from .forms import Message_Form
from .models import JadwalPribadi

response = {'author' : 'Angelina Condro'}

# Create your views here.
def index(request):
	return render(request, 'index.html')

def favorite(request):
	return render(request, 'favorite.html')
	
def challenge(request):
	return render(request, 'challenge.html')

# def jadwal(request):
# 	html = 'jadwal.html'
# 	response['message_form'] = Message_Form()
# 	return render(request, html, response)

def postnew(request):
	form = Message_Form(request.POST)
	if(request.method == 'POST'):
		response['course_name'] = request.POST['course_name']
		response['course_date'] = request.POST['course_date']
		response['course_place'] = request.POST['course_place']
		response['course_category'] = request.POST['course_category']
		jadwal = JadwalPribadi(course_name=response['course_name'], course_date=response['course_date'],
				course_place=response['course_place'],course_category=response['course_category'])
		jadwal.save()
		html ='jadwal.html'
		sched = JadwalPribadi.objects.all()
		context = {
			'message_form': form,
			'sched' : sched
		}
		return render(request, html, context)
	else:
		form = Message_Form()
		sched = JadwalPribadi.objects.all()
		context = {
			'message_form' : form,
			'sched' : sched
		}
		return render(request, 'jadwal.html', context)

def postdelete(request):
    hapus = JadwalPribadi.objects.all().delete()
    form = Message_Form()
    context =    {
		'hapus' : hapus,
		'message_form' : form
    }
    return render(request, 'jadwal.html' , context)



	